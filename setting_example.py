KEY_SUB_NAME = 'sub_name'
KEY_DESTINATION = 'destination'
KEY_LIMIT = 'limit'

DEFAULT_DESTINATION = 'save_dir'
DEFAULT_LIMIT = 10

settings = {}

# Please update following login information.
LOGIN_CLIENT_ID = ''
LOGIN_CLIENT_SECRET = ''
LOGIN_USER = ''
LOGIN_PASSWORD = ''


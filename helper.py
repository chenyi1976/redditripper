import json
import logging
import os
import re
import urllib2

import praw
import time
from imgurpython import ImgurClient

__url_file_map = None
__urls_failed = None

DEFAULT_URLS_FILE = 'urls_map.json'
DEFAULT_URLS_FAIL_FILE = 'urls_fail.json'


def url_exist(uri):
    global __url_file_map
    if __url_file_map is None:
        try:
            file_load = open(DEFAULT_URLS_FILE, 'r')
            __url_file_map = json.load(file_load)
            file_load.close()
        except (IOError, ValueError):
            __url_file_map = {}

    return uri in __url_file_map


def add_url_file(uri_map):
    global __url_file_map
    if __url_file_map is None:
        try:
            file_load = open(DEFAULT_URLS_FILE, 'r')
            __url_file_map = json.load(file_load)
            file_load.close()
        except IOError:
            __url_file_map = []

    previous_len = len(__url_file_map)
    __url_file_map.update(uri_map)
    if len(__url_file_map) != previous_len:
        try:
            file_write = open(DEFAULT_URLS_FILE, 'w+')
            json.dump(__url_file_map, file_write)
            file_write.close()
        except IOError:
            logging.error('cannot write file: {}'.format(DEFAULT_URLS_FILE))


def add_url_failed(uris):
    global __urls_failed
    if __urls_failed is None:
        try:
            file_load = open(DEFAULT_URLS_FAIL_FILE, 'r')
            __urls_failed = json.load(file_load)
            file_load.close()
        except (IOError, ValueError):
            __urls_failed = []

    previous_len = len(__urls_failed)
    for uri in uris:
        if uri not in __urls_failed:
            __urls_failed.append(uri)
    if len(__urls_failed) != previous_len:
        try:
            file_write = open(DEFAULT_URLS_FAIL_FILE, 'w+')
            json.dump(__urls_failed, file_write)
            file_write.close()
        except IOError:
            logging.error('cannot write file: {}'.format(DEFAULT_URLS_FAIL_FILE))


def download_and_save(uri, filename):
    if url_exist(uri):
        logging.warn("url already exist, ignored")
        return
    exist_filename = None
    while os.path.exists(filename):
        exist_filename = filename
        file_base_name, file_extension = os.path.splitext(filename)
        filename = file_base_name + time.strftime('_%m%d%H%M%S') + file_extension
    try:
        pic = urllib2.urlopen(uri)
        wf = open(filename, 'w+b')
        wf.write(pic.read())
        wf.close()
        pic.close()
        if exist_filename:
            if os.path.getsize(filename) == os.path.getsize(exist_filename):
                os.remove(filename)
                logging.info('file already exist, ignored.')
                return
        logging.info('image saved : {}'.format(filename))
        return {uri: filename}
    except urllib2.HTTPError as e:
        logging.error('HTTPError = ' + str(e.code))
        return
    except urllib2.URLError as e:
        logging.error('URLError = ' + str(e.reason))
        return
    except Exception:
        import traceback
        logging.error('generic exception: ' + traceback.format_exc())
        return


def download_pic_from_direct_img_link(uri, save_dir):
    f_name = None
    f_ext = None

    list_result = re.findall("(http|https)://(.+)/(\w+)(\.gif|\.jpg|\.png|)?v?", uri)
    if list_result:
        result = list_result[0]
        if result:
            if len(result) == 4:
                f_name = result[2]
                f_ext = result[3]

    if not f_name:
        logging.error("can not get file name from uri")
        return

    if not f_ext:
        logging.warn('image not found, ignore')
        return

    if uri.endswith('gifv'):
        uri = uri[:-1]
    filename = os.path.join(save_dir, f_name + f_ext)
    return download_and_save(uri, filename)


IMGUR_CLIENT_ID = '3fdf34b6a47a03e'
IMGUR_CLIENT_SECRET = '949127565c06823e2172fa2a3641bdd31cf45862'

PATTERN_IMGUR = "(http|https)://imgur.com/(.+)"
PATTERN_IMGUR_GALLERY = "(http|https)://imgur.com/gallery/(\w+)/?.*"
PATTERN_IMGUR_ALBUM = "(http|https)://imgur.com/a/(\w+)/?.*"
PATTERN_IMGUR_IMAGE = "(http|https)://imgur.com/(\w+)/?.*"


def download_pic_from_imgur(uri, save_dir):
    client = ImgurClient(IMGUR_CLIENT_ID, IMGUR_CLIENT_SECRET)

    parse_list = re.findall(PATTERN_IMGUR, uri)
    if not parse_list:
        return

    image_uri_list = []
    parse_list = re.findall(PATTERN_IMGUR_GALLERY, uri)
    if parse_list:
        gallery_id = parse_list[0][1]
        try:
            client.get_image(gallery_id)
            image_uri_list.append("http://i.imgur.com/" + gallery_id + ".jpg")
        except:
            images = client.get_album_images(gallery_id)
            for im in images:
                image_uri_list.append("http://i.imgur.com/" + im.id + ".jpg")
    else:
        parse_list = re.findall(PATTERN_IMGUR_ALBUM, uri)
        if parse_list:
            album_id = parse_list[0][1]
            images = client.get_album_images(album_id)
            for im in images:
                image_uri_list.append("http://i.imgur.com/" + im.id + ".jpg")
        else:
            parse_list = re.findall(PATTERN_IMGUR_IMAGE, uri)
            if parse_list:
                image_id = parse_list[0][1]
                image_uri_list.append("http://i.imgur.com/" + image_id + ".jpg")
    if not image_uri_list:
        return

    result = {}
    for image_uri in image_uri_list:
        uri_to_file = download_pic_from_direct_img_link(image_uri, save_dir)
        if uri_to_file:
            result.update(uri_to_file)
    return result


PATTERN_GFYCAT = "(http|https)://gfycat.com/(\w+)"


def download_pic_from_gfycat(uri, save_dir):
    parse_list = re.findall(PATTERN_IMGUR, uri)

    if not parse_list:
        return
    else:
        gfycat_id = parse_list[0][1]
        image_uri = "http://gfycat.com/" + gfycat_id + ".gif"
        return download_pic_from_direct_img_link(image_uri, save_dir)


download_handlers = (download_pic_from_imgur, download_pic_from_gfycat, download_pic_from_direct_img_link, )


def download_pic_from_uri(uri, save_dir):
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)

    for handler in download_handlers:
        uri_file_map = handler(uri, save_dir)
        if uri_file_map:
            return uri_file_map


def get_up_voted(redditor, login_user, login_password, client_id, client_secret, limit, destination, download=False):

    r = praw.Reddit(client_id=client_id,
                    client_secret=client_secret,
                    password=login_password,
                    user_agent='https://bitbucket.org/chenyi1976/redditripper',
                    username=login_user)
    up_votes = r.redditor(redditor).upvoted(limit=limit)

    logging.info('--------begin to get from upvote of {}--------'.format(redditor))

    url_file_map = {}
    for idx, up_vote in enumerate(up_votes):
        logging.info('index {}: {}'.format(idx, up_vote.url))
        if download:
            download_result = download_pic_from_uri(up_vote.url, destination)
            if download_result:
                url_file_map.update(download_result)

    logging.info('--------done to get from upvote of {}--------'.format(redditor))

    if download:
        add_url_file(url_file_map)

    return url_file_map


def get_sub(sub_name, login_user, login_password, client_id, client_secret, limit, destination, download=False):
    r = praw.Reddit(client_id=client_id,
                    client_secret=client_secret,
                    password=login_password,
                    user_agent='https://bitbucket.org/chenyi1976/redditripper',
                    username=login_user)

    logging.info('--------begin to get from sub {}----------'.format(sub_name))

    url_file_map = {}
    url_failed_set = set()
    hots = r.subreddit(sub_name).hot(limit=limit)
    for idx, submission in enumerate(hots):
        logging.info('index {}: {}'.format(idx, submission.url))
        if download:
            download_result = download_pic_from_uri(submission.url, destination)
            if download_result:
                url_file_map.update(download_result)
            else:
                url_failed_set.add(submission.url)

    logging.info('--------done to get from sub {}----------'.format(sub_name))

    if download:
        add_url_file(url_file_map)
        if url_failed_set:
            add_url_failed(url_failed_set)

    return url_file_map

# README #

### What is this repository for? ###

This is a tool to download picture from reddit

### Requirement ###


```
#!python

pip install praw
pip install imgurpython

```

### How to run ###

Firstly, rename setting_example.py to setting.py, then update the username and password to your one. 

Some examples:


```
#!python

python tool.py sub -s pics -l 100
python tool.py upvote -u user_name -p user_pass -l 100

```

Script to test.

```
#!python

python tool.py test
python tool.py test -s funny
python tool.py test -s funny -l 20

```
import argparse
import sys
import time

from helper import *

try:
    from setting import *
except ImportError:
    from setting_example import *


def init_logging():
    logging_dir = 'logging'
    if not os.path.exists(logging_dir):
        os.makedirs(logging_dir)

    logging.basicConfig(filename='{}/rr_{}.log'.format(logging_dir, time.strftime('%m%d%H%M')), level=logging.DEBUG)
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    logging.getLogger('').addHandler(console)


if __name__ == '__main__':

    if LOGIN_CLIENT_ID in (None, '') or LOGIN_CLIENT_SECRET in (None, '') \
            or LOGIN_USER in (None, '') or LOGIN_PASSWORD in (None, ''):
        print('please set your login information in settings')
        sys.exit()

    parser = argparse.ArgumentParser()
    parser.add_argument("mode", help="mode, supported value: upvote / sub")

    parser.add_argument("-r", "--redditor", help="redditor name")
    parser.add_argument("-s", "--sub_name", help="sub reddit name (sub mode only)")
    parser.add_argument("-d", "--destination", help="destination folder, default is ./save_dir/")
    parser.add_argument("-l", "--limit", help="limit of entries, default is 10")
    parser.add_argument("-t", "--test", action='store_true', help="do not download anything")
    args = parser.parse_args()

    mode = args.mode

    if mode not in ('upvote', 'sub'):
        print('mode value should be one of: upvote / sub')
        sys.exit()

    limit = args.limit
    try:
        limit = int(limit)
    except (TypeError, ValueError):
        limit = DEFAULT_LIMIT

    destination = args.destination if args.destination else DEFAULT_DESTINATION

    # validation
    if mode == 'sub' and args.sub_name in (None, ""):
        print('sub_name is needed for "sub" mode.')
        print('example: python main.py sub -s pics')
        sys.exit()
    elif mode == 'upvote' and args.redditor in (None, ''):
        print('redditor is needed for "upvote" mode.')
        print('python main.py upvote -r redditor_name')
        sys.exit()

    init_logging()
    if mode == 'upvote':
        logging.info('upvote')
        urls = get_up_voted(args.redditor,
                            LOGIN_USER,
                            LOGIN_PASSWORD,
                            LOGIN_CLIENT_ID,
                            LOGIN_CLIENT_SECRET,
                            limit,
                            destination,
                            download=not args.test)
    elif mode == 'sub':
        logging.info('sub')
        urls = get_sub(args.sub_name,
                       LOGIN_USER,
                       LOGIN_PASSWORD,
                       LOGIN_CLIENT_ID,
                       LOGIN_CLIENT_SECRET,
                       limit,
                       destination,
                       download=not args.test)
